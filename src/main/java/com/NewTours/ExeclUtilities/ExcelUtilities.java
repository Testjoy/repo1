package com.NewTours.ExeclUtilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;


public class ExcelUtilities {

	static XSSFWorkbook wb; // this is POI library reference to a work book
	static XSSFSheet sheet; // this is POI library reference to a sheet

	public static String getURL(String filePath, int Row, int Column, String Sheet) throws IOException{
		File src = new File(filePath);
		FileInputStream fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet=wb.getSheet(Sheet);
		String data = sheet.getRow(Row).getCell(Column).getStringCellValue();
		System.out.println("Value= " + data);
		return data;
	}
	
	public static void writeresult(String filePath, int Row, int Column, int Sheet, String Value) throws IOException{
		File src = new File(filePath);
		FileInputStream fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		
		
		
		sheet=wb.getSheetAt(Sheet);
		//if(Sheet!=0)
		int rownum =0;
		rownum = sheet.getLastRowNum();
		//System.out.println("Last row### "+rownum);
		
		if (rownum < Row)	
		{
			/*System.out.println("Inside IF");
			System.out.println("Last row : "+rownum);
			System.out.println("Row :"+Row);
			System.out.println("Creating new Row");*/
		sheet.createRow(Row);
		
		}
		
		sheet.getRow(Row).createCell(Column).setCellValue(Value);
		
		FileOutputStream fout=new FileOutputStream(new File(filePath));
		wb.write(fout);
		//System.out.println("Test Case Passed");
	
	}
	
	public static String getData(String filepath, int sheetnumber, int rownumber, int colnum) throws IOException {
		//System.out.println("Inside getData method");
		File src = new File(filepath);
		FileInputStream fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet = wb.getSheetAt(sheetnumber);
	
		//System.out.println("Column###"+colnum);
		
		DataFormatter formatter = new DataFormatter();
		String data = formatter.formatCellValue(sheet.getRow(rownumber).getCell(colnum));
		//System.out.println("Cell Data ###"+data);
		
		return data;
		
	}
	
	
	public int getRowCount(int sheetIndex) {
		System.out.println("Inside getRowsCount method");
		int rowcount = wb.getSheetAt(sheetIndex).getLastRowNum();
		return rowcount;
	}
	
}	
