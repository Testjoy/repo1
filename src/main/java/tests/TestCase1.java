package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

public class TestCase1 {
	public WebDriver driver;
	
  @Test
  public void openMyBlog() {
	driver.get("https://www.facebook.com/");
  }
  
  @BeforeClass
  public void beforeClass() {
	  
	  System.setProperty("webdriver.gecko.driver", "C:\\\\Users\\\\norma\\\\Documents\\\\Lyret\\\\Selenium\\\\geckodriver-v0.26.0-win64 (1)\\\\geckodriver.exe");
	  driver = new FirefoxDriver();
	  
  }

  @AfterClass
  public void afterClass() {
	  driver.quit();
  }
}
